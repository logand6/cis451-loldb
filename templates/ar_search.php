

<?php
include('connectionData.txt');
$conn = mysqli_connect($server, $user, $pass, $dbname, $port)
or die('Error connecting to MySQL server.');
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset='utf-8'> 
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="League of friends">
		<meta name="author" content="Douglas Uyeda">
		
		<title> Attribute/Role Search </title>
        
		<!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	
        <!-- JQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        
		<!-- Custom CSS -->
		<link href = "../css/style.css" rel = "stylesheet">
		
	</head>

    <?php
    $attribute = $_POST['attribute'];
    $role = $_POST['role'];
    $query = "SELECT champ_id, champ_name, role_name, attr_name
                FROM champion
                JOIN champion_has_role USING(champ_id)
                JOIN champion_has_attribute USING(champ_id)
                JOIN role USING(role_id)
                JOIN attribute USING(attr_id)
                WHERE attr_name = ? AND role_name = ?;";
    ?>
    
    <body>
        <!-- Nav Bar -->
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="champ_search.html">League of Friends</a>
            </div>            
        </div>       
    </nav>
    
    <div class = "container">
        <div class = "panel panel-default">
            <div class = "panel-body">
                <div class = "page-header">
                    <h3> You searched for: </h3>
                </div>
                <div>
                    <?php
                    print $attribute;
                    print "<br>";
                    print $role;
                    ?>
                </div>
                
                <div class = "page-header">
                    <h3> The Query </h3>
                </div> 
                <div>
                    <?php
                        print $query;
                    ?>
                </div>
                
                <div class = "page-header">
                    <h3> Result of Query </h3>
                </div>
                <div>
                    <?php
                        print "<pre>";
                        if ($stmt = $conn->prepare($query)) {
                            $stmt->bind_param("ss", $attribute, $role); 
                            $stmt->execute();

                            /* bind variables to prepared statement */
                            $stmt->bind_result($col1, $col2, $col3, $col4);

							printf("%s %20s %20s %20s\n","Champ ID","Champ Name", "Attribute", "Role");
							printf("__________________________________________________________________________\n");
                            /* fetch values */
                            while ($stmt->fetch()) {
                                printf("%-18d %-23s %-20s %-20s\n", 
                                $col1, $col2, $col3, $col4);
                            }
                            /* close statement */
                            $stmt->close();
                        }
                        print "</pre>";
                        $conn->close();
                    ?>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Footer -->   
    <div class = "navbar navbar-fixed-bottom footer-style">
        <center><p> Copyright © 2015 League of Friends </p></center>
    </div>

    </body>
</html>
