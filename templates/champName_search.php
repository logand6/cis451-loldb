<?php
include('connectionData.txt');
$conn = mysqli_connect($server, $user, $pass, $dbname, $port)
or die('Error connecting to MySQL server.');
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset='utf-8'> 
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="League of friends">
		<meta name="author" content="Douglas Uyeda">
		
		<title> Champion Name Search </title>
        
		<!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	
        <!-- JQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        
		<!-- Custom CSS -->
		<link href = "../css/style.css" rel = "stylesheet">
		
	</head>

    <?php
    $champName = $_POST['champName'];
    $champName =  ucwords(strtolower($champName));
    $query = "SELECT *
              from champion 
              WHERE champ_name like ?;";
    ?>
    
    <body>
        <!-- Nav Bar -->
    <nav class="navbar navbar-inverse">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="champ_search.html">League of Friends</a>
            </div>            
        </div>       
    </nav>
    
    <div class = "container">
        <div class = "panel panel-default">
            <div class = "panel-body">
                <div class = "page-header">
                    <h3> You searched for: </h3>
                </div>
                <div>
                    <?php
                    print $champName;
                    ?>
                </div>
                
                <div class = "page-header">
                    <h3> The Query </h3>
                </div> 
                <div>
                    <?php
                        print $query;
                    ?>
                </div>
                
                <div class = "page-header">
                    <h3> Result of Query </h3>
                </div>
                <div>
                    <?php
                        print "<pre>";
                        if ($stmt = $conn->prepare($query)) {
                            $stmt->bind_param("s", $champName); 
                            $stmt->execute();

                            /* bind variables to prepared statement */
                            $stmt->bind_result($col1, $col2, $col3, $col4, $col5, $col6, $col7, $col8);
                            /* fetch values */
                            while ($stmt->fetch()) {
                                printf("Champ ID: %d\n Champ Name: %s\n Passive: %s\n Q: %s\n W: %s\n E: %s\n R: %s\n IP COST: %d\n", 
                                $col1, $col2, $col3, $col4, $col5, $col6, $col7, $col8);
                            }
                            /* close statement */
                            $stmt->close();
                        }
                        print "</pre>";
                        $conn->close();
                    ?>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Footer -->   
    <div class = "navbar navbar-fixed-bottom footer-style">
        <center><p> Copyright © 2015 League of Friends </p></center>
    </div>

    </body>
</html>
