<?php
include('connectionData.txt');
$conn = mysqli_connect($server, $user, $pass, $dbname, $port)
or die('Error connecting to MySQL server.');
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset='utf-8'> 
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="League of friends">
		<meta name="author" content="Douglas Uyeda">
		
		<title> IP Search </title>
        
		<!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	
        <!-- JQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        
		<!-- Custom CSS -->
		<link href = "../css/style.css" rel = "stylesheet">
		
	</head>

    <?php
    $ip_cost = $_POST['ip_cost'];
    $int_ip_cost = (int)$ip_cost;
    $sort_by = $_POST['sort_by'];  
    if($sort_by == 'ASC'){
        $query = "SELECT champ_id, champ_name, champ_ip_cost
                FROM champion
                JOIN champion_has_role USING(champ_id)
                JOIN champion_has_attribute USING(champ_id)
                WHERE champ_ip_cost <= ?
                ORDER BY champ_ip_cost ASC";
    }
    elseif($sort_by == 'DESC'){
        $query = "SELECT champ_id, champ_name, champ_ip_cost
                FROM champion
                JOIN champion_has_role USING(champ_id)
                JOIN champion_has_attribute USING(champ_id)
                WHERE champ_ip_cost <= ?
                ORDER BY champ_ip_cost DESC";
    }
    ?>
    
    <body>
        <!-- Nav Bar -->
    <nav class="navbar navbar-inverse">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="champ_search.html">League of Friends</a>
            </div>            
        </div>       
    </nav>
    
    <div class = "container">
        <div class = "panel panel-default">
            <div class = "panel-body">
                <div class = "page-header">
                    <h3> You searched for: </h3>
                </div>
                <div>
                    <?php
                    echo $int_ip_cost;
                    print "<br>";
                    echo $sort_by;
                    ?>
                </div>
                
                <div class = "page-header">
                    <h3> The Query </h3>
                </div> 
                <div>
                    <?php
                        print $query;
                    ?>
                </div>
                
                <div class = "page-header">
                    <h3> Result of Query </h3>
                </div>
                <div>
                    <?php
                        print "<pre>";
                        if ($stmt = $conn->prepare($query)) {
                            $stmt->bind_param("i", $int_ip_cost); 
                            $stmt->execute();

                            /* bind variables to prepared statement */
                            $stmt->bind_result($col1, $col2, $col3);
							printf("%s %20s %20s\n","Champ ID","Champ Name", "IP Cost");
							printf("_____________________________________________________\n");
                            /* fetch values */
                            while ($stmt->fetch()) {
                                printf("%-18d %-23s %-20d\n", $col1, $col2, $col3);
                            }
                            /* close statement */
                            $stmt->close();
                        }
                        print "</pre>";
                        $conn->close();
                    ?>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Footer -->   
    <div class = "navbar navbar-fixed-bottom footer-style">
        <center><p> Copyright © 2015 League of Friends </p></center>
    </div>

    </body>
</html>
