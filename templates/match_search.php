<?php
include('connectionData.txt');
$conn = mysqli_connect($server, $user, $pass, $dbname, $port)
or die('Error connecting to MySQL server.');
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset='utf-8'> 
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="League of friends">
		<meta name="author" content="Douglas Uyeda">
		
		<title> Summoner Match Search </title>
        
		<!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	
        <!-- JQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        
		<!-- Custom CSS -->
		<link href = "../css/style.css" rel = "stylesheet">
		
	</head>

    <?php
    $sumMatch = $_POST['sumMatch'];
    $query = "SELECT game_date, sum_name, sum_level, map_name, type_description, champ_name
                FROM summoner
                JOIN participant USING(sum_id)
                JOIN game USING(game_id)
                JOIN game_map ON game.game_map_id = game_map.map_id
                JOIN game_type ON game.game_type_id = game_type.type_id
                JOIN game_mode ON game.game_mode_id = game_mode.mode_id
                JOIN champion USING(champ_id)
                WHERE sum_name = ?;";
    ?>
    
    <body>
        <!-- Nav Bar -->
    <nav class="navbar navbar-inverse">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="summoner_search.html">League of Friends</a>
            </div>            
        </div>       
    </nav>
    
    <div class = "container">
        <div class = "panel panel-default">
            <div class = "panel-body">
                <div class = "page-header">
                    <h3> You searched for: </h3>
                </div>
                <div>
                    <?php
                    print $sumMatch;
                    ?>
                </div>
                
                <div class = "page-header">
                    <h3> The Query </h3>
                </div> 
                <div>
                    <?php
                        print $query;
                    ?>
                </div>
                
                <div class = "page-header">
                    <h3> Result of Query </h3>
                </div>
                <div>
                    <?php
                        print "<pre>";
                        if ($stmt = $conn->prepare($query)) {
                            $stmt->bind_param("s", $sumMatch); 
                            $stmt->execute();

                        	
							 /* bind variables to prepared statement */
                            $stmt->bind_result($col1, $col2, $col3, $col4, $col5, $col6);
                            printf("%s %30s %15s %20s %28s %20s\n","Date","Summoner Name", "Level", "Map Name", "Type Description", "Champion Name");
							printf("___________________________________________________________________________________________________________________________\n");
                            /* fetch values */
                            while ($stmt->fetch()) {
                            	
                                printf("%-22s %-23s %-15d %-22s %-22s %-12s\n", 
                               $col1, $col2, $col3, $col4, $col5, $col6);
                            }
                            /* close statement */
                            $stmt->close();
                        }
                        print "</pre>";
                        $conn->close();
                    ?>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Footer -->   
    <div class = "navbar navbar-fixed-bottom footer-style">
        <center><p> Copyright � 2015 League of Friends </p></center>
    </div>

    </body>
</html>
