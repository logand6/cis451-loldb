CIS 451 Fall 2015
Final Project
Team Members: Logan Donielson, Douglas Uyeda, Darrekk Hocking.

League of Legends Database

Table of Contents:

1. Where to find the database
2. Summary
	a. Description of database
	b. Types of data to be stored
	c. application programs desired
3. Logical Design
4. Physical Design
5. List of Applications
6. User's Guide
7. Contents of Tables
8. Implementation Code
9. Conclusion

||-----------------------------------------||
	1. Where to find the database
||-----------------------------------------||


For this project, we will be using BitBucket for version control, and storing all project files.
	https://bitbucket.org/logand6/cis451-loldb


The PHP web app to use the database is located on ix.

	https://ix.cs.uoregon.edu/~duyeda/cis451/cis451-loldb/templates/champ_search


There is no password for the BitBucket repository or the web app.


The database itself is located on ix with the following connection details.

	server: ix.cs.uoregon.edu
	database name: LOLDB
	username: guest
	password: guest
	port 3664


If guest does not work for user/pass, you can also the following credentials.

	user: logand
	password: user

Sample input and output can be found in sample.txt, which is on the BitBucket repo.

||-----------------------------------------||
		2. Summary
||-----------------------------------------||

	a. Description of Database
	Our LOLDB database is intended to model data about the game League of Legends. 
    	League of Legends, (LOL), is a type of game known as a MOBA (Multiplayer online battle arena). 
    	In the game, the players, or "Summoners", choose characters, "Champions", to battle each other 
    	with in a variety of game types and maps. The player's profile is a major part of the game, 
    	as the players themselves can level up, and are ranked based on various statistics (mostly league and division).

	b. Types of data to be stored
	The LOLDB database will store information about Summoners, Champions, maps, game types, and match history. 
    	The Summoner information, (player profile), will include Summoner name, level, league, and division. 
    	Summoners level up from 1-30, and once they are level 30 they can enter into a league. There are multiple leagues (bronze, silver, gold etc.) and multiple divisions (1-5) for each league. Players move up in the leagues and divisions by winning 	ranked league games.
    	The Champions information will include a champion id, name, champion type, role, passive upgrade, four abilities, 
    	and cost in IP, (influence points). The map and game type information will include game modes, 	map ip, and game types.

	c. Application programs desired
	Our main goal for this database is to be able to quickly look up information about fellow summoners to check your and your friend's progress in the leagues. We will do this using a summoner stats search, where you can enter a summoner name, and it 	will return all the info about that summoner. We also want to be able to look up what summoners are in what league, so there 	will also be an application for showing all players in a certain league and division.
	Also, we will have a search application for match history, where you can search for a summoner to retrieve their match 	history, including game type and outcome.
	
	The other main piece of functionality we wanted was to be able to look up champion information to research various abilities, or to quickly find a champion that fits your desired play style, role or budget.
We will do this using a champion search field, where users can enter the name of a champion and get info about the champion. You can also search for champions based on role/attribute, and max ip cost.

||-----------------------------------------||
	      3. Logical Design
||-----------------------------------------||

Logical design can be found in the LOLDB_ERdiagram.pdf file located on the BitBucket repository.

||-----------------------------------------||
	   4. Physical Design
||-----------------------------------------||

Physical design can be found in the dump.sql file located on the BitBucket repository.

||-----------------------------------------||
	  5. List of Applications
||-----------------------------------------||
*********************************************************************************************************
NOTE: Example I/O for each application can be found in sample.txt, which is on the BitBucket repository.
*********************************************************************************************************

	Summoner Search:
	
	Search summoner stats:
	The search bar for summoner stats is used to look up summoners (players) by name to find information on their leveling and  	progress in their league.
	Input: summoner name
	Output: summoner ID, summoner name, level, league, and division.
	This application only affects the summoner table.
	
	Search summoner by league-division:
	The search by league-division field is used to look up which players are in which league-division. For instance, challenger is the highest level, and 1 is the highest division, so to see the top players, you could select challenger for 	league and 1 for division.
	Input: league, division
	Output: summoner ID, summoner name, level, league, and division.
	This application only affects the summoner table.

	Search match history:
	The search match history field is used to look up the match history of the summoner. This is used to check out player activity, such as how often they play, or which champion they play the most.
	Input: summoner name
	Output: The date, summoner name, level, map name, type description, and champion for each game played by the summoner searched for.
	This application affects the summoner, participant, game, game_map, game_type, game_mode, and champion tables.
	


	Champion Search:
	
	Search by champion name:
	The search by champion name field is used to look up any champion to see their abilities and cost to by. This can be used to learn about various champions abilities and descriptions.
	Input: champion name
	Output: champ id, champ name, passive ability, q ability, w ability, e ability, r ability, and ip cost.
	This application only affects the champion table.

	Search by champion Role/Attribute:
	The search by champion Role/Attribute is used to look up champions that have a particular play style, and are suited to a certain team position. This could be used by someone who likes to play Mages, but also needs to play in the top lane.
	Input: attribute, role
	Output: The champ id, champ name, attribute, and role of all champions that have both the attribute and role specified.
	This application affects the champion, champion_has_role, champion_has_attribute, role, and attribute tables

	Search by champion IP Cost:
	The search by champion IP cost field is used to shop for champions. The user will specify their maximum budget, in IP, and choose whether they want to sort by ascending or descending. This lets players filter through large numbers of champions 	to find one they like and can afford, without searching for individual champions.
	Input: maximum ip, and either ascending or descending sort by.
	Output: The champ id, champ name, and ip cost of every champion below the max amount of ip, sorted in the order specified by the user.
	This application affects the champion, champion_has_role, and champion_has_attribute tables.

||-----------------------------------------||
	      6. User's Guide
||-----------------------------------------||

The League of Friends (LOLDB) will help you find information on both summoners and champions.

In summoner search, you can look up fellow summoners to find their progress in the leagues and their match history. You can also find a list of summoners in each league-division.
In champion search, you can look up champions by name to see their passive, abilities, and cost to purchase, in ip. You can also look up champions by their role and attribute, for example all assassins who are mid. Finally, you can shop for champions filtered max ip cost and sorted in either ascending or descending order.


Summoner Search:
	
Search summoner stats:
To search for a summoner's stats, simply enter in the name of a summoner, for example "smeb", then hit enter, or click the blue search icon to the right of the text entry field. You will then be able to see the summoner's id, name, level, league, and division.
		
Search summoner by league-division:
To search for summoners in a particular league-division, simply choose a league, (challenger, platinum, etc), from the Select League drop down menu, then select a division, (5,4,3, etc), from the Select Division drop down menu. Then, press the submit button below the Select Division bar to bring up a list of all summoners in the specified league-division.

Search match history:
To search for a summoner's match history, simply enter in the name of a summoner, for example "smeb", then hit enter, or click the blue search icon to the right of the text entry field. You will then be able to see a list of the games the summoner has played, with information about each game, including date, duration, game type, champion played, etc.


Champion Search:	

Search by champion name:
To search for a champion by name, simply enter in the name of a champion, for example "tristana", then hit enter, or click the blue search icon to the right of the text entry field. You will then be able to see the champion's id, name, passive, all 4 abilities, and cost to purchase in ip.

Search by champion Role/Attribute:
To search for a champion by role/attribute, choose an attribute (Support, Mage, etc) from the Select Attribute drop down menu, then select a role, (Mid, ADC, etc), from the Select Role drop down menu. You will then be able to see a list of champions that fit both the attribute and role specified.

Search by champion IP Cost:
To search for champions by ip cost, choose a maximum cost from the MAX IP drop down menu, (450 IP, 4800 IP, etc), then select how you want to sort the champions with the Sort by drop down menu. Then, press the submit button below the Sort By drop down menu to display a list of champions less than or equal to the maximum specified cost, and sorted in the order chosen.


||-----------------------------------------||
	   7. Contents of Tables
||-----------------------------------------||

Contents of tables can be found in the dump.sql file located on the BitBucket repository.

||-----------------------------------------||
	   8. Implementation Code
||-----------------------------------------||

Implementation code can be found on the BitBucket repository.

||-----------------------------------------||
	       9. Conclusion
||-----------------------------------------||

What we have done with The League of Friends, aka LOLDB, is create a web app with 2 main pieces of functionality. The first is summoner search, which lets us search for our fellow players (summoners) by name. Summoner search can also retrieve a list of all summoners in a particular league-division, as well as look up the match history of any summoner.
The second is champion search, which lets us search for characters (champions) to play by name. Champion search returns detailed information about the champion, including their cost, and a description of all their abilities. Champion search can also retrieve a list of champions base on role/attribute, so you can find a champion based on what role or attribute you like best. Finally, there is a shop-like feature where you can search for champions based on their cost to purchase. This is done by choosing a maximum cost and sorting option.

If we had more time to develop this project, we would have liked to make it look a bit nicer, and add some icons and backgrounds that fit with the theme of the game. Also, we would have liked to implement a feature to track summoners win/loss ratio, and a slightly more advanced ranking system. Finally, it would've been nice to add more to the search champion by cost feature, so you could filter champions by cost in other currencies as well as a better interface for filtering, sorting, and displaying the champions.